<?php

namespace Drupal\patternlabloader\Template\Loader;

use Drupal\Core\Extension\ThemeHandlerInterface;
use FabbDev\PatternLab\PatternPartialLoader\Twig\Loader\PatternPartialLoader as PatternPartialLoaderOriginal;
use Psr\Log\LoggerInterface;

/**
 * Loads pattern lab patterns included with the shorthand format.
 *
 * This just extends the core partial loader to set the paths up correctly in
 * Drupal.
 *
 * @see \Drupal\patternlabloader\Template\Loader\PatternLab\PatternPartialLoader
 * @see https://patternlab.io/docs/pattern-including.html#the-shorthand-include-syntax
 *
 * @todo Add caching if necessary.
 */
class PatternPartialLoader extends PatternPartialLoaderOriginal {

  const CONFIG_KEY = 'pattern-lab-loader';

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  public function __construct(ThemeHandlerInterface $themeHandler, LoggerInterface $logger) {
    $this->logger = $logger;
    $this->themeHandler = $themeHandler;

    $patternPathData = $this->getPatternPathData();
    parent::__construct($patternPathData['paths'], ['patternPaths' => $patternPathData['patternPaths']]);
  }

  /**
   * Returns the pattern root and pattern paths for use with the core loader.
   *
   * @return array
   *   An associative array keyed by:
   *   - paths: An array of paths for the core loader constructor;
   *   - patternPaths: An array of pattern paths for the loader constructor.
   */
  protected function getPatternPathData() {
    $configs = $this->getThemeInfoConfig();
    $config = $configs->current();

    // There's currently no support for multiple data files.
    $configs->next();
    if ($configs->current()) {
      $this->logger->warning("More than one info file with the @key key was found but only one will be used.", ['@key' => static::CONFIG_KEY]);
    }

    $encoded = file_get_contents($config['file_path']);
    if ($encoded === FALSE) {
      $this->logger->error("Failed to read file $config[file_path]");
      return [];
    }

    $data = json_decode($encoded, TRUE);
    if (json_last_error() != JSON_ERROR_NONE) {
      $this->logger->error("Failed to decode JSON from $config[file_path]");
      return [];
    }

    return [
      'paths' => [$config['pattern_root']],
      'patternPaths' => $data,
    ];
  }

  /**
   * Yield all plugin configuration found in theme info files.
   *
   * @return \Generator|array
   *   A generator that yields an associative array keyed by:
   *   - file_path: The path relative to the site root of the data file
   *     containing the pattern paths;
   *   - pattern_root: The root directory for the patterns relative to the
   *     theme; this is where the paths in the JSON file above will be resolved
   *     from.
   */
  protected function getThemeInfoConfig() {
    foreach ($this->themeHandler->listInfo() as $name => $extension) {
      $info = $extension->info;

      $data_file = $this->getInfoValue($info, 'data_file');
      if ($data_file) {
        $pattern_root = $extension->getPath();

        $pattern_root_suffix = $this->getInfoValue($info, 'pattern_root');
        if ($pattern_root_suffix) {
          $pattern_root .= '/' . $pattern_root_suffix;
        }

        yield [
          'pattern_root' => $pattern_root,
          'file_path' => $extension->getPath() . '/' . $data_file,
        ];
      }
    }
  }

  /**
   * Return a plugin config value from an info file array.
   *
   * @param array $info
   *   The info file array for a theme or module.
   * @param string $name
   *   The name of the config value to return.
   *
   * @return mixed|null
   *   The config value if found; NULL otherwise.
   */
  protected function getInfoValue(array $info, $name) {
    return isset($info[static::CONFIG_KEY][$name]) ? $info[static::CONFIG_KEY][$name] : NULL;
  }

}
